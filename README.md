## Introduction

We have created a development environment based on "building blocks" that are reusable packages, not necessarily WordPress plugins, that are pulled and "glued" together using [Composer](https://getcomposer.org/).

We have developed a few packages that we use in enterprise WordPress projects, and that we have made open source.

They are:

- [Inpsyde Composer VIP plugin](https://celeritassolutions@bitbucket.org/celeritassolutions/vip-composer-plugin.git)
- [Composer Asset Compiler](https://bitbucket.org/celeritassolutions/composer-asset-compiler/)
- [WP App Container](https://bitbucket.org/celeritassolutions/wp-app-container)

We suggest you read those packages' documentation because you will briefly integrate them for your certification task.

## Your task

The task is for to write a WordPress **child-theme** skeleton.

You *don't* need to create a fully-featured child theme, with many templates and intricate styles, but:

- a basic `style.css` that might not contain any CSS at all, but just the [theme headers](https://developer.wordpress.org/themes/advanced-topics/child-themes/#2-create-a-stylesheet-style-css)
- a `functions.php` that you will use as a starting point to integrate with two packages: "**MAH Dashboard**" and "**MAH Search**"

Inside the `/__packages/` folder, this repository contains the two mentioned packages and the parent theme you have to use as your child theme's parent.

The three packages in the repository: the parent theme, MAH Dashboard, and MAH Search, are used are proof-of-concept packages, so don't expect all best practices applied or great code there.

### The parent theme

The **parent theme** is almost empty. It contains a single template that features the MAH logo, a paragraph of text, and a search form. It won't display the content of any post or page: all the URLs always render the same fixed HTML.

The only reason for this theme to existing is to create a child theme for it.

Your child theme will need to override the `/parts/before-content.php` template of the parent theme to **add a link below the search form** that, once clicked, will show an "*Advanced search*" form in place of the standard search form. However, **this link has to be shown only if "Advanced search" is enabled**.

### The dashboard package

**MAH Dashboard** is a fictional building block that provides other plugins and themes to accept users' configuration in the WordPress dashboard. Think of it like an extensible "setting page" that plugins and themes can use to centralize the settings they need.

You have to use this package to create **a single setting** for your child theme. That only configuration you have to code is about enabling *Advanced search*. You will create e a simple *Yes/No* toggle that you can implement as a radio button, dropdown, or in any other way you find appropriate.

If the users choose *Yes*, the "advanced search" functionality will be active; otherwise, it will not be.

In the README located inside `/__packages/mah-dashboard/` folder, you can find documentation on creating a setting that integrates with the MAH Dashboard package.

### The search package

**MAH Search** is a fictional package that provides advanced search functionality for a website. This package provides a method to print an advanced search form in a template. You'll need to use this method to **print the advanced search form in the template of your child theme, in the case "Advanced search" is activated**.

Moreover, when this package is in use and the advanced search form is submitted, a custom template is used to print the search results. That custom "search results" template can be provided by themes and child themes or will default to a template file located in the package folder.

Your task is to provide the advanced "search results" template via your child theme, alongside some (basic) CSS styling for it.

In the README located inside `/__packages/mah-search/` folder, you can find documentation on how to print the advanced search form and override the default template from your child theme.

### Integrating Composer

Because the child theme you'll write will be integrated into the website repository managed via Composer, it will need to support Composer. The requirement can be fulfilled by creating a `composer.json`. Please make sure to use the correct package "type" among those supported by [Composer installers](https://github.com/composer/installers). You can have a look at the `composer.json` used by the parent theme in this repo for reference.

### Integrating Asset Compiler

To complete the task described above, you'll need to write some styles for the advanced search form and the search results template.

You might also need to write some JavaScript to switch from the *standard* search form to the *advanced* search form when users click the "Advanced Search" link you'll print in the template.

Considering the simplicity of the CSS and JS needed, there would be no strict need for CSS and JS pre/post-processing. However, we'd like you to write styles using a CSS pre or post-processor (SCSS, Less, Stylus, PostCSS) and write Javascript using ES6 or more recent versions, then "compile" both CSS and JS for the browser using a tool of your choice. You could use a task runner such as Grunt or Gulp or a module bundler such as Webpack.

If you need external dependencies, please use npm or Yarn to pull them, and do not download 3rd party libraries into your theme folder.

The "compiled" CSS and JS should **not** be shipped with your theme, but you need to have a "script" in your `packages.json` that launches the building process. This script needs then to be configured in the theme's `composer.json` in the format supported by [Composer Asset Compiler](https://github.com/inpsyde/composer-asset-compiler#assets-building-scripts).

### A step-by-step guide

For your convenience, here's a step-by-step guide of what we expect to complete the task:

1. Fork the current repository in a **private** repository of your own. Please double check that your fork is *not* publicly accessible. Then, clone your fork locally.
2. Inside the `/__packages/` folder, create **a sub-folder for your child-theme**. In it, make sure there's a `style.css` containing theme headers and a `composer.json` providing package name and type (Look at `/__packages/mah-theme/composer.json` for reference).
3. In the `composer.json` in the root folder of the repository, require your child theme. You can take an example from how the other three packages inside the `/__packages/` folder are required.
4. Use Composer and Inpsyde Composer VIP plugin to set up your repository for local development. The Composer VIP plugin has [quite detailed documentation](https://github.com/inpsyde/vip-composer-plugin#folder-structure-after-the-command-is-ran). In a nutshell, you have to run: `composer update && composer vip --local`. When those commands complete their work (it will take *some* time), there will be a `wp-config.php` in the repository root to set configuration, such as DB credentials.
5. Create a local development environment, using a tool of your choice. The requirement is a LAMP/LEMP stack (with PHP 7.3+), but how to obtain that is up to you. You could use Docker, Vagrant, XAMPP/MAMP, or anything else. Just make sure to point your web-server's webroot to the `/public/` folder that the Composer VIP plugin created for you.
6. Referring to the README in `/__packages/mah-dashboard/`, write the necessary PHP code to create a setting widget to enable or disable "Advanced Search". To add the hook essential to integrate into the *MAH Dashboard* package, use a `functions.php` to be placed in your child theme folder.
7. Create a template file in your child theme to override the `/parts/before-content.php` template in the parent theme. In your template, make sure to print the default search form (already in the parent theme), followed by a link that users can click to hide the standard search form and show the advanced search form. Make sure the link is only visible if the advanced search is enabled. To print the advanced search form, make use of the *MAH Search* package. Refer to the README in `/__packages/mah-search/` for documentation.
8. Create a template file in your child theme to be used to show advanced search results. This template needs to override the search results template used by the *MAH Search* package. Refer to the package's README for documentation.
9. Write styles, and eventually scripts, that you need for the two templates you have written (the "before content" and the "search results"). Make sure to have a [`packages.json` with a "scripts" section](https://classic.yarnpkg.com/en/docs/package-json/#toc-scripts) that exposes a single command necessary to build your theme styles and scripts. Add configuration in your theme's `composer.json` that [Composer Asset Compiler](https://github.com/inpsyde/composer-asset-compiler#assets-building-scripts) can use to compile your assets. Ensure the "compiled" assets are enqueued in WordPress when necessary, by using [WordPress functionality](https://developer.wordpress.org/reference/functions/wp_enqueue_style/).
10. When done with the steps above, update Composer and Composer VIP plugin (one-liner: `composer update && composer vip --local`) and make sure your theme is made available in WordPress and it works as expected when activated.
11. If there's information or special instructions you want to provide regarding your child theme, include those in a README in your child theme's root folder.
12. Make sure "compiled" assets for your theme are being Git-ignored and push the changes to your repository fork. Contact your reference person at MAH, and provide them access to your repository (read-only access is sufficient). You might be asked to provide access to additional users.
13. Congratulations, your task is completed.
