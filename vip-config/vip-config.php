<?php

/**
 * This is the network main configuration file that is loaded automatically and very early,
 * think of it as the equivalent of `wp-config.php` in VIP Go environment.
 */

const WP_DEFAULT_THEME = 'agency-cert-theme';
