<?php
/**
 * Plugin Name: Inpsyde Application bootstrap
 */

declare(strict_types=1);

namespace Mah;

use Inpsyde\App;

/**
 * Helper to reliable get a singleton instance of `App`.
 * @return App\App
 */
function app(): App\App {
    static $app;
    if (!$app) {
        $app = App\App::new(new App\Container(new App\EnvConfig('Mah\\Vip\\Config')));
    }

    return $app;
}

app()
    ->addProvider(new Dashboard\Provider(), App\Context::BACKOFFICE, App\Context::AJAX)
    ->addProvider(new Search\Provider(), App\Context::FRONTOFFICE)
    ->boot();
