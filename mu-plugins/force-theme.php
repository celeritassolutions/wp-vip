<?php

/**
 * Plugin Name: Ensure theme
 */

declare(strict_types=1);

if (defined('WP_INSTALLING') && WP_INSTALLING) {
    return;
}

if (!wp_get_theme()->exists()) {
    update_option('stylesheet', 'agency-cert-theme');
    update_option('template', 'agency-cert-theme');
}
