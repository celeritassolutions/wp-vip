<?php

declare(strict_types=1);

namespace Mah\Dashboard;

use Brain\Nonces\WpNonce;

class Dashboard
{
    public const ACTION_REGISTER = 'mah.dashboard.register-pages';

    /**
     * @var array<Page>
     */
    private $pages = [];

    /**
     * @param Page $page
     * @return $this
     */
    public function addPage(Page $page): Dashboard
    {
        $this->pages[] = $page;

        return $this;
    }

    /**
     * @return void
     */
    public function register()
    {
        add_action(
            'admin_init',
            function () {
                if (wp_doing_ajax()) {
                   $this->registerAjaxActions();
                }
            }
        );

        add_action(
            'admin_menu',
            function () {
                if (wp_doing_ajax()) {
                    return;
                }

                do_action(self::ACTION_REGISTER, $this);
                if (!$this->pages) {
                    return;
                }

                add_menu_page(
                    __('MAH Dashboard', 'mah-dashboard'),
                    __('MAH Dashboard', 'mah-dashboard'),
                    'manage_categories',
                    'mah-dashboard',
                    function () {
                        $this->renderStart();
                        $this->printPages();
                        $this->renderEnd();
                    },
                    'dashicons-forms',
                    99
                );
            }
        );
    }

    /**
     * @return void
     */
    private function renderStart(): void
    {
        ?>
        <div class="wrap">
        <h1><?= esc_html__('MAH Dashboard', 'mah-dashboard') ?></h1>
        <?php
    }

    /**
     * @return void
     */
    private function renderEnd(): void
    {
        ?>
        </div>
        <script>
            (function ($) {
                $('form.mah-dashboard-widget__form').on('submit', function (e) {
                    e.preventDefault();
                    var $form = $(this),
                        $submit = $form.find('p.submit input'),
                        $content = $form.children('div').eq(0),
                        errorMsg = '<?= esc_js(__("Error saving widget.", 'mah-dashboard')) ?>',
                        okMsg = '<?= esc_js(__("Widget saved.", 'mah-dashboard')) ?>',
                        notice = function (error, msg) {
                            var color = error ? 'red' : 'green',
                                msgTxt = error ? (msg || errorMsg) : okMsg;
                            $content.prepend(
                                '<p class="mah-notice" style="color:' + color + ';">' + msgTxt + '</p>'
                            );
                        };
                    $submit.prop('disabled', true);
                    $.ajax({
                        url: ajaxurl,
                        cache: false,
                        method: 'POST',
                        data: $(this).serialize()
                    }).done(function(data) {
                        if (!data || !(data.success || null)) {
                            notice(true, data.data.error || null);
                            return;
                        }
                        notice(false);
                    }).fail(function () {
                        notice(true);
                    }).always(function() {
                        $submit.prop('disabled', false);
                        setTimeout(function () {
                            $form.find('div .mah-notice').remove();
                        }, 3500);
                    });
                });
            })(jQuery);
        </script>
        <?php
    }

    /**
     * @return void
     */
    private function printPages(): void
    {
        $pageId = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
        $current = null;
        $currentUrl = null;
        $links = [];
        $base_url = add_query_arg('page', 'mah-dashboard', admin_url('admin.php'));

        /** @var Page $page */
        foreach ($this->pages as $page) {
            $slug = $page->slug();
            if (
                !$slug
                || !$page->enabled()
                || !current_user_can($page->capability() ?? 'manage_options')
            ) {
                continue;
            }

            $url = add_query_arg('id', $slug, $base_url);
            if ($current === null || ($slug === $pageId)) {
                $current = $page;
                $currentUrl = $url;
            }

            $links[$url] = $page->title();

            if ($page === null || ($page->slug() === $page)) {
                $current = $page;
            }
        }

        if (!$links) {
            printf(
                '<div style="margin-top:2em"><p style="text-align:center">%s</p></div>',
                esc_html__('No configuration widgets registered.', 'mah-dashboard')
            );

            return;
        }

        $this->renderTitles($links, $currentUrl);
        $this->renderPage($current);
    }

    /**
     * @param array $links
     * @param string $currentUrl
     * @return void
     */
    private function renderTitles(array $links, string $currentUrl): void
    {
        if (count($links) === 1) {
            return;
        }

        ?>
        <div style="margin: 1em auto">
            <table style="border-left:1px solid #7e8993; margin: 0 12px">
                <tr>
                    <?php foreach ($links as $url => $title) : ?>
                        <td style="padding:0 12px; border-right:1px solid #7e8993">
                            <h3 style="font-size:1.1em; padding:0; margin: 0">
                            <?php if ($url === $currentUrl) : ?>
                                <?= esc_html($title) ?>
                            <?php else : ?>
                                <a href="<?= esc_url($url) ?>"><?= esc_html($title) ?></a>
                            <?php endif ?>
                            </h3>
                        </td>
                    <?php endforeach ?>
                </tr>
            </table>
        </div>
        <?php
    }

    /**
     * @param Page $page
     * @return void
     */
    private function renderPage(Page $page): void
    {
        do_action(Page::ACTION_SETUP, $page);
        $widgets = $page->widgets();
        if (!$widgets) {
            printf(
                '<div style="margin-top:2em"><p style="text-align:center">%s</p></div>',
                esc_html__('No configuration widgets registered for this page.', 'mah-dashboard')
            );
            return;
        }

        print '<div style="margin: 0 auto"><table class="form-table"><tbody>';
        $defCap = $page->capability() ?? 'manage_options';
        $i = 0;
        $open = false;
        /** @var Widget $widget */
        foreach ($widgets as $widget) {
            $slug = $widget->slug();
            if (
                !$slug
                || !$widget->enabled()
                || !current_user_can($widget->capability() ?? $defCap)
            ) {
                continue;
            }

            $i++;
            $id = sanitize_html_class($slug);
            $nonce = new WpNonce("mah-dashboard-{$id}");
            if ($i % 2 === 1) {
                print('<tr>');
                $open = true;
            }
            ?>
            <td>
                <div class="mah-dashboard-widget mah-dashboard-widget--<?= $id ?> postbox">
                    <div class="postbox-header" style="padding:12px;font-size:.75em;height:1em">
                        <h2><?= esc_html($widget->title()) ?></h2>
                    </div>
                    <form
                        class="mah-dashboard-widget__form"
                        id="mah-dashboard-widget-form-<?= $id ?>">
                        <input
                            type="hidden"
                            name="action"
                            value="mah-dashboard-<?= $id ?>">
                        <input
                            type="hidden"
                            name="mah-dashboard-page-id"
                            value="<?= esc_attr($page->slug()) ?>">
                        <?= \Brain\Nonces\formField($nonce) ?>
                        <div class="inside">
                            <?php $widget->render() ?>
                            <p style="text-align:right;padding-right:14px">
                                <?php
                                submit_button(__('Save', 'mah-dashboard'), '', 'submit', false);
                                ?>
                            </p>
                        </div>
                    </form>
                </div>
            </td>
            <?php
            if ($i % 2 === 0) {
                print('</tr>');
                $open = false;
            }
        }
        if ($open) {
            print('<td style="width:50%">&nbsp;</td></tr>');
        }
        print '</tbody></table></div>';
    }

    /**
     * @return void
     */
    private function registerAjaxActions(): void
    {
        $action = filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING);
        $pageId = filter_input(INPUT_POST, 'mah-dashboard-page-id', FILTER_SANITIZE_STRING);
        if (!$action || ! $pageId || (strpos($action, 'mah-dashboard-') !== 0)) {
            return;
        }

        $target = substr($action, strlen('mah-dashboard-'));
        $nonce = new WpNonce($action);
        if (!$target || !$nonce->validate()) {
            wp_send_json_error();
        }

        do_action(self::ACTION_REGISTER, $this);

        /** @var Page $page */
        foreach ($this->pages as $page) {
            $slug = $page->slug();
            $capability = $page->capability() ?? 'manage_options';

            if (($slug !== $pageId) || !$page->enabled() || !current_user_can($capability)) {
                continue;
            }

            do_action(Page::ACTION_SETUP, $page);

            /** @var Widget $widget */
            foreach ($page->widgets() as $widget) {
                $slug = $widget->slug();
                if (
                    ($slug === $target)
                    && $widget->enabled()
                    && current_user_can($widget->capability() ?? $capability)
                ) {
                    $this->saveWidget($widget);
                }
            }
        }
    }

    /**
     * @param Widget $widget
     * @return void
     */
    private function saveWidget(Widget $widget): void
    {
        try {
            $widget->save();
            wp_send_json_success();
        } catch (\Throwable $throwable) {
            wp_send_json_error(['error' => esc_html($throwable->getMessage())]);
        }
    }
}
