<?php

declare(strict_types=1);

namespace Mah\Dashboard;

class BasePage implements Page
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $slug;

    /**
     * @var string
     */
    private $capability;

    /**
     * @var callable|null
     */
    private $enabler;

    /**
     * @var array<Widget>
     */
    private $widgets = [];

    /**
     * @param string $title
     * @param string $capability
     * @param string|null $slug
     * @param callable|null $enabler
     */
    public function __construct(
        string $title,
        string $capability = 'edit_others_pages',
        ?string $slug = null,
        ?callable $enabler = null
    ) {

        $this->title = $title;
        $this->slug = $slug ?? sanitize_title_with_dashes($title);
        $this->capability = $capability;
        $this->enabler = $enabler;
    }

    /**
     * @return string
     */
    public function slug(): string
    {
        return $this->slug;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->title;
    }

    /**
     * @return string|null
     */
    public function capability(): ?string
    {
        return $this->capability;
    }

    /**
     * @return bool
     */
    public function enabled(): bool
    {
        return $this->enabler ? ($this->enabler)() : true;
    }

    /**
     * @param Widget $widget
     * @return Page
     */
    public function addWidget(Widget $widget): Page
    {
        $this->widgets[] = $widget;

        return $this;
    }

    /**
     * @return array
     */
    public function widgets(): array
    {
        return $this->widgets;
    }
}
