<?php

declare(strict_types=1);

namespace Mah\Dashboard;

class ExampleWidget implements Widget
{
    /**
     * @return void
     */
    public function render(): void
    {
        $value = get_option('mah-dashboard-example-widget-text', '');
        ?>
        <table class="table-form" style="width:100%">
            <tr>
                <th scope="row">
                    <label for="example-widget-text">
                        <?= esc_html__('Please enter some text', 'mah-dashboard') ?>
                    </label>
                </th>
                <td>
                    <input
                        class="large-text"
                        type="text"
                        id="example-widget-text"
                        name="example-widget-text"
                        value="<?= esc_attr($value) ?>">
                </td>
            </tr>
        </table>
        <?php
    }

    /**
     * @return void
     */
    public function save(): void
    {
        $input = sanitize_text_field(wp_slash($_POST['example-widget-text'] ?? ''));
        if (strpos($input, 'Wordpress')) {
            throw new \Exception('WordPress must be written with capital P.');
        }

        $input
            ? update_option('mah-dashboard-example-widget-text', $input, false)
            : delete_option('mah-dashboard-example-widget-text');

    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Example Widget';
    }

    /**
     * @return string
     */
    public function slug(): string
    {
        return 'example-widget';
    }

    /**
     * @return string|null
     */
    public function capability(): ?string
    {
        // Null to inherit capability from page
        return null;
    }

    /**
     * @return bool
     */
    public function enabled(): bool
    {
        return true;
    }
}
