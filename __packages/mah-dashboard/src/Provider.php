<?php

declare(strict_types=1);

namespace Mah\Dashboard;

use Inpsyde\App\Container;
use Inpsyde\App\Provider\Booted;

class Provider extends Booted
{
    /**
     * @param Container $container
     * @return bool
     */
    public function register(Container $container): bool
    {
        $container->addService(
            Dashboard::class,
            static function (): Dashboard {
                return new Dashboard();
            }
        );

        $container->addService(
            'mah-dashboard-default-page',
            static function (): Page {
                return new BasePage(
                    __('Welcome to MAH Dashboard', 'mah-dashboard'),
                    'edit_others_pages',
                    '__default'
                );
            }
        );

        return true;
    }

    /**
     * @param Container $container
     * @return bool
     */
    public function boot(Container $container): bool
    {
        add_action(
            Dashboard::ACTION_REGISTER,
            static function (Dashboard $dashboard) use ($container): void {
                $dashboard->addPage($container->get('mah-dashboard-default-page'));
            },
            PHP_INT_MIN
        );

        add_action(
            Page::ACTION_SETUP,
            static function (Page $page) use ($container): void  {
                if ($page->slug() === '__default') {
                    $page->addWidget(new ExampleWidget());
                }
            }
        );

        $container->get(Dashboard::class)->register();

        return true;
    }
}
