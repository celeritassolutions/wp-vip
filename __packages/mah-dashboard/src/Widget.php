<?php

declare(strict_types=1);

namespace Mah\Dashboard;

interface Widget
{
    /**
     * Render the widget HTML.
     *
     * @return void
     */
    public function render(): void;

    /**
     * Save the widget.
     *
     * @return void
     */
    public function save(): void;

    /**
     * @return string
     */
    public function title(): string;

    /**
     * @return string
     */
    public function slug(): string;

    /**
     * Return null to inherit capability from page.
     *
     * @return string|null
     */
    public function capability(): ?string;

    /**
     * Return false to hide the widget.
     *
     * @return bool
     */
    public function enabled(): bool;
}
