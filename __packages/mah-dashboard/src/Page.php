<?php

declare(strict_types=1);

namespace Mah\Dashboard;

interface Page
{
    public const ACTION_SETUP = 'mah.dashboard.page-setup';

    /**
     * @return string
     */
    public function slug(): string;

    /**
     * @return string
     */
    public function title(): string;

    /**
     * @return string|null
     */
    public function capability(): ?string;

    /**
     * @return bool
     */
    public function enabled(): bool;

    /**
     * @param Widget $widget
     * @return static
     */
    public function addWidget(Widget $widget): Page;

    /**
     * @return array<Widget>
     */
    public function widgets(): array;
}
