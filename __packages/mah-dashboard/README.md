# MAH Dashboard

Example package for dashboard configuration for agency certification of MAH websites.

----


## What is this and how to use it

This package provides other packages/plugins/themes a centralized place for dashboard settings.

Settings are organized in "Pages", and each page provides one or more "Widgets".

The package has a *default page* where it is possible to add widgets.

Each page is an instance of the interface `Mah\Dashboard\Page`. The package also ships a class, `Mah\Dashboard\BasePage`, that is an implementation of that interface, and it is used by the package itself to add the *default page*.

Each widget is an instance of the interface `Mah\Dashboard\Widget`. The package ships an example implementation in the class `Mah\Dashboard\ExampleWidget`.



### Adding widgets

To add a widget to a page, you have to use the `Page::addWidget()` method, which is made available listening to the `'mah-dashboard .page-setup'` action hook that passes a `Page` instance as an argument.

For example, this is how the package itself adds the example widget to the default page:

```php
use Mah\Dashboard\{Page, ExampleWidget};

add_action(
    Page::ACTION_SETUP, // 'mah.dashboard.page-setup'
    static function (Page $page): void {
        if ($page->slug() === '__default') {
            $page->addWidget(new ExampleWidget());
        }
    }
);
```



### Adding pages

To add a new page, you have to use the  ` 'mah.dashboard.register-pages'` action hook that passes an instance of `Mah\Dashboard\Dashboard`: an object having an` addPage` method.

For example, this is how the package itself adds the default page:

```php
use Mah\Dashboard\{Dashboard, BasePage};

add_action(
    Dashboard::ACTION_REGISTER, // 'mah.dashboard.register-pages'
    static function (Dashboard $dashboard): void {
        $dashboard->addPage(
            new BasePage(
                __('Welcome to MAH Dashboard', 'mah-dashboard'),
                'edit_others_pages',
                '__default'
            )
        );
    }
);
```

Note how the third parameter passed to `BasePage` constructor is `'_default'`, which is the same ID checked in the previous snippet to add the example widget to the default page.
