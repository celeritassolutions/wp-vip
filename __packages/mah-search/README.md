# MAH Search

Example "advanced search" package for agency certification of MAH websites.

---



## What is this

In MAH wp.com VIP environments, there is an "advanced search" plugin that enhances WordPress search functionalities.

This package, explicitly created for the agency certification process, is a placeholder for that real thing, and provides two proof-of-concept functionalities:

- A `Search::printAdvancedSearchForm()` static method that, as its name suggests, prints a search form for the fictional "advanced search" functionality
- When a user submits the "advanced search" form, a custom template located at `/templates/advanced-search.php` is used instead of the default WordPress search template (`search.php`). Themes and child themes can override that custom template. More on this below.



## Search results template override

(Child)Themes can override the `advanced-search.php` template that the package uses by creating a template file and saving it in the path `mah-search/advanced-search.php` inside their root folder.



## Advanced search form HTML

The search form HTML printed by the `Search::printAdvancedSearchForm()` method is not hardcoded, but it depends on the current theme (or child theme) in use, specifically, on the `searchform.php` template in there.

If the (child)theme does not ship a `searchform.php` file, then the method will return the default WordPress search form HTML (see [get_search_form documentation](https://developer.wordpress.org/reference/functions/get_search_form/)).
