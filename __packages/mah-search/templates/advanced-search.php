<?php
get_header('advanced-search');

?>
    <header>
        <h1 class="page-title">
            <?php
            esc_html_e(
                sprintf(
                    __('Search results for "%s"', 'mah-search'),
                    get_search_query(false)
                )
            );
            ?>
        </h1>

        <section class="search-form">
            <?php \Mah\Search\Search::printAdvancedSearchForm() ?>
        </section>
    </header>

    <section class="main">
	    <p>Displaying advanced search results below.</p>
        <?php
        if (!have_posts()) {
            if (!locate_template('mah-search/no-results.php', true, false)) {
                ?>
                <p><?= esc_html__('No results found.', 'mah-search') ?></p>
                <?php
            }
        } else {
            while (have_posts()) {
                the_post();
                // look into theme override first
                if (!locate_template('mah-search/entry.php', true, false)) {
                    ?>
                    <article class="<?php post_class('search-result') ?>" id="<?php the_ID() ?>">
                        <h2><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h2>
                        <div class="summary"><?php the_excerpt() ?></div>
                    </article>
                    <?php
                }
            }
        }
        ?>
    </section>
<?php

get_footer('advanced-footer');
