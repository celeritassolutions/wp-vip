<?php

declare(strict_types=1);

namespace Mah\Search;

class Search
{
    private const QUERY_VAR = 'adv-search';

    /**
     * @return void
     */
    public static function printAdvancedSearchForm(): void
    {
        static $html5;
        $html5 or $html5 = static function (): string {
            return 'html5';
        };
        add_filter('search_form_format', $html5, PHP_INT_MAX);
        $html = get_search_form(['echo' => false]);
        remove_filter('search_form_format', $html5, PHP_INT_MAX);

        $dom = new \DOMDocument();
        $dom->loadHTML("<html><body>{$html}</body></html>");
        $forms = $dom->getElementsByTagName('form');
        $form = ($forms->count() === 1) ? $forms->item(0) : null;
        if (!$form || !$form instanceof \DOMElement) {
            return;
        }

        $class = $form->getAttribute('class') . ' advanced-search-form';
        $form->setAttribute('class', trim($class));

        $el = $dom->createElement('input');
        $el->setAttribute('type', 'hidden');
        $el->setAttribute('name', self::QUERY_VAR);
        $el->setAttribute('value', '1');

        $form->appendChild($el);

        print $dom->saveHTML($form);
    }

    /**
     * @return void
     */
    public function override(): void
    {
        add_action(
            'pre_get_posts',
            static function (\WP_Query $query) {
                if (
                    is_admin()
                    || !$query->is_main_query()
                    || !$query->is_search()
                    || !filter_var($_REQUEST[self::QUERY_VAR] ?? false, FILTER_VALIDATE_BOOLEAN)
                ) {
                    return;
                }

                // Templates can do `if (get_query_var('advanced-search'))` to check as
                // sort-of conditional tag.
                $query->set('advanced-search', true);

                add_filter(
                    'template_include',
                    static function () {
                        $file = locate_template('mah-search/advanced-search.php');
                        if ($file) {
                            return $file;
                        }

                        return dirname(__DIR__) . '/templates/advanced-search.php';
                    },
                    PHP_INT_MAX
                );
            }
        );
    }
}
