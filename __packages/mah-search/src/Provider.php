<?php

declare(strict_types=1);

namespace Mah\Search;

use Inpsyde\App\Container;
use Inpsyde\App\Provider\Booted;

class Provider extends Booted
{
    /**
     * @param Container $container
     * @return bool
     */
    public function register(Container $container): bool
    {
        $container->addService(
            Search::class,
            static function (): Search {
                return new Search();
            }
        );

        return true;
    }

    /**
     * @param Container $container
     * @return bool
     */
    public function boot(Container $container): bool
    {
        $container->get(Search::class)->override();

        return true;
    }
}
