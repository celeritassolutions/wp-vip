<?php if (is_search()): ?>
    <section class="main">
        <p><?= esc_html__('Default search is boring.', 'mah-theme') ?></p>
        <p><strong><?= esc_html__('Maybe try advanced search?', 'mah-theme') ?></strong></p>
    </section>
<?php else: ?>
<section class="main">
    <p>
        <?= esc_html__('Welcome to MAH theme for agency certification.', 'mah-theme') ?>
    </p>
</section>
<?php
endif;
