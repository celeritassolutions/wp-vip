<?php
get_header();
get_template_part('parts/before-content');
get_template_part('parts/content');
get_template_part('parts/after-content');
get_footer();
