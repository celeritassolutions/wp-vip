<?php $adv_search_enabled = get_option('mah-dashboard-adv-search'); ?>

<section class="search-form <?php echo $adv_search_enabled; ?>">
    <?php
	    if ($adv_search_enabled == 'enabled') {?>
	    	<div class="adv-search-wrapper">
			    <?php
				\Mah\Search\Search::printAdvancedSearchForm(); 
				?>
				<p>You are using the advanced search form.</p>
	    	</div>
			<?php
		} else { ?>
			<div class="basic-search-wrapper">
				<?php
				get_search_form();
				?>
				<p>You are using the default search form. <a class="show-adv-search">Click here if you would like to use the advanced search form.</a></p>
			</div>
			<div class="adv-search-wrapper">
				<?php Mah\Search\Search::printAdvancedSearchForm(); ?>
				<p>You are using the advanced search form.</p>
			</div>
			
			</div>
		<?php }
    ?>
</section>
