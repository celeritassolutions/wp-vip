<?php

namespace Mah\Dashboard;
	
class SearchWidgetEx implements Widget
{
    /**
     * @return void
     */
    public function render(): void
    {
        $value = get_option('mah-dashboard-adv-search', '');

        ?>
        <table class="table-form" style="width:100%">
            <tr>
                <th scope="row">
                    <label for="adv-search">
                        <?= esc_html__('Use advanced search?', 'mah-dashboard') ?>
                    </label>
                </th>
                <td>
	                <input type="radio" id="adv-search-enabled" name="adv-search" value="enabled" <?php if($value == 'enabled') echo 'checked="checked"'; ?>>
					<label for="adv-search-enabled">Yes</label><br>
					<input type="radio" id="adv-search-disabled" name="adv-search" value="disabled" <?php if($value == 'disabled') echo 'checked="checked"'; ?>>
					<label for="adv-search-disabled">No</label><br>
                </td>
            </tr>
        </table>
        <?php
    }

    /**
     * @return void
     */
    public function save(): void
    {
        $input = $_POST['adv-search'];

        $input
            ? update_option('mah-dashboard-adv-search', $input, false)
            : delete_option('mah-dashboard-adv-search');

    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Advanced Search';
    }

    /**
     * @return string
     */
    public function slug(): string
    {
        return 'adv-search';
    }

    /**
     * @return string|null
     */
    public function capability(): ?string
    {
        // Null to inherit capability from page
        return null;
    }

    /**
     * @return bool
     */
    public function enabled(): bool
    {
        return true;
    }
}


use Mah\Dashboard\{Page, SearchWidget};

add_action(
    Page::ACTION_SETUP, // 'mah.dashboard.page-setup'
    static function (Page $page): void {
        if ($page->slug() === '__default') {
            $page->addWidget(new SearchWidgetEx());
        }
    }
);

