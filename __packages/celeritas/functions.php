<?php
	
require_once( get_stylesheet_directory() . '/src/SearchWidget.php');

function as_adding_scripts() {
	wp_register_script('adv-search-js', get_stylesheet_directory_uri() . '/js/adv-search.js', array('jquery'),'1.1', true);
	wp_enqueue_script('adv-search-js');
}
add_action( 'wp_enqueue_scripts', 'as_adding_scripts' ); 
