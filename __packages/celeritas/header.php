<!DOCTYPE html>
<html lang="<?php language_attributes() ?>">
<head>
    <meta charset="<?php bloginfo('charset') ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link
        rel="icon"
        href="https://www.merck-animal-health.com/wp-content/uploads/2019/12/cropped-favicon-1.png?w=32"
        sizes="32x32"/>
    <link
        rel="icon"
        href="https://www.merck-animal-health.com/wp-content/uploads/2019/12/cropped-favicon-1.png?w=192"
        sizes="192x192"/>
    <link
        rel="apple-touch-icon"
        href="https://www.merck-animal-health.com/wp-content/uploads/2019/12/cropped-favicon-1.png?w=180"/>

    <?php
    wp_enqueue_style(get_stylesheet(), get_stylesheet_uri());
    wp_head();
    ?>
</head>
<body>
<header class="main-header">
    <p><?php get_template_part('parts/logo') ?></p>
</header>
